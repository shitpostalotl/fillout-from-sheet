{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  nativeBuildInputs = [
    pkgs.python3
    pkgs.gnumeric
    pkgs.python310Packages.openpyxl
    pkgs.python310Packages.pyflakes
  ];
}
