import openpyxl, re, copy

sheet = openpyxl.load_workbook(filename = input("Which file? ")+'.xlsx')[input("Which sheet? ")]
out = ""
pattern = input("Pattern: ")
DollarPercent = 1

for i in range(input("Sheet has header? [Y/N] ").lower()=="y",len(sheet["a"])+1):
	workingpattern = copy.deepcopy(pattern)
	while "$$" or "$^" in workingpattern: # replace with cell contents
		symbol = {"True":"$$","False":"$^"}[str("$$" in workingpattern)]
		try:
			match = re.search(r"\$(\$|\^)[a-zA-Z]", workingpattern).group()
			val = sheet[match[2].upper()][i].value
			workingpattern = workingpattern.replace(match,val[:len(val)*(symbol=="$$")+1])
		except: break
	while "$%" in workingpattern: # replace with iteratng number that resets upon change in cell contents
		match = re.search(r"\$\%[a-zA-Z]", workingpattern).group()
		try: DollarPercent = DollarPercent*(sheet[match[2]][i].value==sheet[match[2]][i-1].value)+1
		except: break
		workingpattern = workingpattern.replace(match,str(DollarPercent))
	out = out + workingpattern + "\n"

fout = open("out","w")
fout.write("\n".join(out.split("\n")[:-2]))
fout.close()